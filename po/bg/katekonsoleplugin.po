# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Yasen Pramatarov <yasen@lindeas.com>, 2011.
# SPDX-FileCopyrightText: 2022, 2023, 2024 Mincho Kondarev <mkondarev@yahoo.de>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-25 00:40+0000\n"
"PO-Revision-Date: 2024-04-29 09:30+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 24.02.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kateconsole.cpp:70
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr "Нямате достатъчно карма за достъп до обвивка или терминална емулация"

#: kateconsole.cpp:118 kateconsole.cpp:148 kateconsole.cpp:769
#, kde-format
msgid "Terminal"
msgstr "Терминал"

#: kateconsole.cpp:157
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "&Пренасочване към терминала"

#: kateconsole.cpp:161
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "&Синхронизиране на терминала с текущия документ"

#: kateconsole.cpp:165
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "Изпълняване на текущия документ"

#: kateconsole.cpp:170 kateconsole.cpp:556
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "По&казване на панела на терминала"

#: kateconsole.cpp:176
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "Фокусиране на терминален панел"

#: kateconsole.cpp:182
#, kde-format
msgctxt "@action"
msgid "&Split Terminal Vertically"
msgstr "&Вертикално разделяне"

#: kateconsole.cpp:187
#, kde-format
msgctxt "@action"
msgid "&Split Terminal Horizontally"
msgstr "&Хоризонтално разделяне"

#: kateconsole.cpp:192
#, kde-format
msgctxt "@action"
msgid "&New Terminal Tab"
msgstr "&Стартиране на терминал в нов раздел"

#: kateconsole.cpp:355
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr ""
"Konsole не е инсталирана. Моля, инсталирайте konsole, за да можете да "
"използвате терминала."

#: kateconsole.cpp:431
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"Наистина ли искате да пренасочите текста към терминала? Всички съдържащи се "
"в него команди ще бъдат изпълнени с вашите права."

#: kateconsole.cpp:432
#, kde-format
msgid "Pipe to Terminal?"
msgstr "Да се пренасочи ли към терминала?"

#: kateconsole.cpp:433
#, kde-format
msgid "Pipe to Terminal"
msgstr "Пренасочване към терминала"

#: kateconsole.cpp:497
#, kde-format
msgid "Not a local file: '%1'"
msgstr "Не-локален файл: '%1'"

#: kateconsole.cpp:530
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"Наистина ли искате да пренасочите текста към терминала?\n"
"Всички съдържащи се в него команди ще бъдат\n"
"изпълнени с вашите права в терминал:\n"
" '%1"

#: kateconsole.cpp:537
#, kde-format
msgid "Run in Terminal?"
msgstr "Да се изпълни ли в терминал?"

#: kateconsole.cpp:538
#, kde-format
msgid "Run"
msgstr "Изпълняване"

#: kateconsole.cpp:553
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "&Скриване на терминален панел"

#: kateconsole.cpp:564
#, kde-format
msgid "Defocus Terminal Panel"
msgstr "Дефокусиране на терминален панел"

#: kateconsole.cpp:565 kateconsole.cpp:566
#, kde-format
msgid "Focus Terminal Panel"
msgstr "Фокусиране на терминален панел"

#: kateconsole.cpp:677
#, kde-format
msgid "Terminal Synchronization Mode"
msgstr "Режим на синхронизиране на терминала"

#: kateconsole.cpp:680
#, kde-format
msgid "&Don't synchronize terminal tab with current document"
msgstr "&Да не се синхронизира терминала с текущия документ"

#: kateconsole.cpp:683
#, kde-format
msgid ""
"&Automatically synchronize the current terminal tab with the current "
"document when possible"
msgstr ""
"&Автоматично синхронизиране на терминала с текущия документ при възможност"

#: kateconsole.cpp:686
#, kde-format
msgid ""
"&Automatically create a terminal tab for the directory of the current "
"document and switch to it"
msgstr ""
"&Автоматично създаване на терминал за директорията на текущия документ и "
"превключване към него"

#: kateconsole.cpp:693 kateconsole.cpp:714
#, kde-format
msgid "Run in terminal"
msgstr "Изпълняване в терминал"

#: kateconsole.cpp:695
#, kde-format
msgid "&Remove extension"
msgstr "&Премахване на разширение"

#: kateconsole.cpp:700
#, kde-format
msgid "Prefix:"
msgstr "Префикс:"

#: kateconsole.cpp:708
#, kde-format
msgid "&Show warning next time"
msgstr "&Показване на предупреждение следващия път"

#: kateconsole.cpp:710
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""
"Следващият път, когато се изпълни \"%1\",   ще се появи предупредителен "
"прозорец, показващ командата, която трябва да бъде изпратена до терминала."

#: kateconsole.cpp:721
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "Променливата на средата &EDITOR да е \"kate -b\""

#: kateconsole.cpp:724
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""
"Важно: За да продължи конзолната програма, документът трябва да бъде "
"затворен."

#: kateconsole.cpp:727
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr "Скриване на конзолата при натискане на 'Esc'"

#: kateconsole.cpp:730
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""
"Това може да доведе до проблеми с терминалните приложения, които използват "
"ESC клавиша, например vim. Добавете тези приложения в полето по-долу (списък "
"със запетая)"

#: kateconsole.cpp:774
#, kde-format
msgid "Terminal Settings"
msgstr "Настройки на терминала"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "&Инструменти"
